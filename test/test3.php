<?php
    $result="";
    if (isset($_POST['btn'])) {
        # code...
        $start=$_POST['start_date'];
        $end=$_POST['end_date'];
        #cal
        $start=date_create($start);
        $end=date_create($end);
        $day=$start->diff($end);
        $result=$day->d."day(s)";

    }   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Make calculator</title>
</head>
<body>
    <h1>Date Difference</h1>
    <form method='POST'>
    <table width="300">
        <tr>
            <td>Start Date</td>
            <td>:</td>
            <td>
                <input type="date" name="start_date">
            </td>
        </tr>
        <tr>
            <td>End Date</td>
            <td>:</td>
            <td>
                <input type="date" name="end_date">
            </td>
        </tr>
        <tr>
            <td>Result</td>
            <td>:</td>
            <td>
                <input type="text" name="result" value=<?php echo $result;?>>
            </td>
        </tr>
        <tr>
          
            <td></td>
            <td></td>
            <td>
                <br>
                <button name="btn">Calculate</Button>
                <button>Cancel</button>
            </td>
        </tr>
    </table>
    </form>
   
    
</body>
</html>