<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Enter URL</h1>
    <form method='POST'>
        INPUT URL :<input type="text" name='url' size='70'>
        <button>GO</button>
    </form>
    <?php
    if(isset($_POST['url'])){
        $url=$_POST['url'];
        if($url==""){
            echo "<p style='color:red; font-size: 30px;'>Please input url</p>";
        }
        else{
            $http=strtr($url,0,7);
            $https=strtr($url,0,8);
            if($http != "http://" && $https != "https://"){
                $url ="https://". $url;
            }
            header('location: '. $url);

        }
    }
    ?>
</body>
</html>